#include "./catch2/catch.hpp"
#include "../framework/CycleManager.h"

TEST_CASE("CycleManagerTests") {
    SECTION("Should return cycle information correctly when iterating through cycles") {
        std::vector<std::pair<int, char>> reads = {{10, 'R'}, {4, 'B'}, {4, 'B'}, {10, 'R'}};

        //calculate cycles to test each read cycle with and
        //calculate cycles that need to be skipped to increment cycles twice, like incrementCurrentCycle() does
        std::vector<int> testCycles = {5, 12, 16, 23};
        std::vector<int> cyclesToSkip = {10, 28};

        CycleManager cycleManager(reads);

        int cycleCount = cycleManager.getCycleCount();
        REQUIRE(cycleCount == 28);

        for (int cycle = 1; cycle <= cycleCount; cycle++) {

            if (cycle == 5) {
                REQUIRE(cycleManager.getCurrentCycle() == 5);
                REQUIRE(cycleManager.getCurrentReadCycle() == 5);
                REQUIRE(cycleManager.getCurrentReadId() == 0);
                REQUIRE(cycleManager.getCurrentReadLength() == 10);
                REQUIRE(!cycleManager.isBarcodeCycle());
                REQUIRE(cycleManager.getCurrentMateId() == 1);
            }

            if (cycle == 12) {
                REQUIRE(cycleManager.getCurrentCycle() == 12);
                REQUIRE(cycleManager.getCurrentReadCycle() == 2);
                REQUIRE(cycleManager.getCurrentReadId() == 1);
                REQUIRE(cycleManager.getCurrentReadLength() == 4);
                REQUIRE(cycleManager.isBarcodeCycle());
                REQUIRE(cycleManager.getCurrentMateId() == 0);
            }

            if (cycle == 16) {
                REQUIRE(cycleManager.getCurrentCycle() == 16);
                REQUIRE(cycleManager.getCurrentReadCycle() == 2);
                REQUIRE(cycleManager.getCurrentReadId() == 2);
                REQUIRE(cycleManager.getCurrentReadLength() == 4);
                REQUIRE(cycleManager.isBarcodeCycle());
                REQUIRE(cycleManager.getCurrentMateId() == 0);
            }

            if (cycle == 23) {
                REQUIRE(cycleManager.getCurrentCycle() == 23);
                REQUIRE(cycleManager.getCurrentReadCycle() == 5);
                REQUIRE(cycleManager.getCurrentReadId() == 3);
                REQUIRE(cycleManager.getCurrentReadLength() == 10);
                REQUIRE(!cycleManager.isBarcodeCycle());
                REQUIRE(cycleManager.getCurrentMateId() == 2);
            }

            cycleManager.incrementCurrentCycle();

            if (cycle == cyclesToSkip.front()) {
                // TODO: Comment back in, when skip Cycle-issue is fully understood
                // cycle++;
                cyclesToSkip.erase(cyclesToSkip.begin()); // Remove first element
            }
        }
    }
}