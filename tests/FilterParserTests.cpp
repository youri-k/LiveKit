#include "./catch2/catch.hpp"
#include "../framework/basecalls/parser/FilterParser.h"
#include "../framework/basecalls/data_representation/FilterRepresentation.h"

using namespace std;

TEST_CASE("FilterParserTests") {
    vector<uint16_t> lanes = {1, 2};
    vector<uint16_t> tiles = {1101};

    FilterRepresentation filterRepresentation(lanes, tiles);
    FilterParser filterParser(lanes, tiles, &filterRepresentation); // 2 lane, 1 tiles
    string path = "../tests/mockFiles/TestBaseCalls";
    // only the filter file for the first lane exists

    SECTION("Should be able to parse single filter file") {
        filterParser.parse(1, 1101, path, 100);

        // check if the correct number of reads were parsed
        REQUIRE(filterRepresentation.getNumSequences(1, 1101) == 100);

        // check if content is correctly parsed
        // only the data at the first position is 0
        REQUIRE(filterRepresentation.getFilterDataForPosition(1, 1101, 0) == 0);
        REQUIRE(filterRepresentation.getFilterDataForPosition(1, 1101, 1) != 0);
        REQUIRE(filterRepresentation.getFilterDataForPosition(1, 1101, 2) != 0);
        // the rest is also 1
    }

    SECTION("Should be able to handle the non-existence of the filter file") {
        // in case the filter file does not exist, none of the data is filtered.
        // Because of this the vector which contains the filterData is initialized with 1's

        filterParser.parse(2, 1101, path, 100);

        // check if the correct number of reads were parsed
        REQUIRE(filterRepresentation.getNumSequences(2, 1101) == 100);

        // check if content is correctly parsed
        REQUIRE(filterRepresentation.getFilterDataForPosition(2, 1101, 0) != 0);
        REQUIRE(filterRepresentation.getFilterDataForPosition(2, 1101, 1) != 0);
        REQUIRE(filterRepresentation.getFilterDataForPosition(2, 1101, 2) != 0);
        // the rest is also 1 (unequal to 0)
    }
}
