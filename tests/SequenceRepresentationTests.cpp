#include "./catch2/catch.hpp"
#include "../framework/basecalls/data_representation/SequenceContainer.h"

TEST_CASE("SequenceContainer") {

    std::vector<char> nucleotides = {'A', 'T', 'C'};

    SECTION("Should set size in all Sequences") {
        SequenceContainer sequences = SequenceContainer();
        sequences.setSize(3, {{20, 'R'}}); // read-structure: 20R
        sequences.extendSequences(nucleotides);
        for (int i = 0; i < 2; i++) {
            Sequence sequence = sequences.getSequence(i);
            //0 because we only want to look at the first saved char in each sequence
            REQUIRE(sequence[0].at(0) == nucleotides[i]);
        }
    }

    SECTION("Should set char in all Sequences for a complex read structure") {
        SequenceContainer sequences = SequenceContainer();
        sequences.setSize(3, {{2, 'R'}, {1, 'B'}, {1, 'B'}, {2, 'R'}}); // read-structure: 2R 1B 1B 2R

        // This is a bit dirty but does the trick:
        // The following lines simulate sequences.extendSequence(0, 'T'), but this method is a private function
        sequences.extendSequences(std::vector<char>(1, 'T'));
        sequences.extendSequences(std::vector<char>(1, 'A'));
        sequences.extendSequences(std::vector<char>(1, 'T'));
        sequences.extendSequences(std::vector<char>(1, 'C'));
        sequences.extendSequences(std::vector<char>(1, 'G'));

        auto sequence = sequences.getSequence(0);

        REQUIRE(sequence[0].size() == 2);
        REQUIRE(sequence[1].size() == 1);
        REQUIRE(sequence[2].size() == 1);
        REQUIRE(sequence[3].size() == 1); // because we only have inserted 5 nucleotides

        REQUIRE(sequence[0] == (Read){'T','A'});
        REQUIRE(sequence[1] == (Read){'T'});
        REQUIRE(sequence[2] == (Read){'C'});
        REQUIRE(sequence[3] == (Read){'G'});
    }

    SECTION("Should establish serializing Size correctly") {
        SequenceContainer sequences;
        sequences.setSize(5, {{20, 'R'}, {4, 'B'}, {20, 'R'}}); // read-structure: 20R

        std::vector<char> longerNucleotides = {'A', 'T', 'C', 'G', 'A'};

        unsigned long baseSize = sizeof(unsigned long);
        // The size of sequences is 5. Each sequence has n = 3 reads
        // Each sequence needs:
        // 1 long for the totalSize
        // 1 long for numReadElements
        // n longs for the maxSize of each Read
        // n longs for the actual size of each Read
        baseSize += 5 * (1 + 1 + 3 + 3) * sizeof(unsigned long);

        for (int i = 1; i <= 3; i++) {
            sequences.extendSequences(longerNucleotides);
            REQUIRE(sequences.serializableSize() == i * longerNucleotides.size() + baseSize);
        }
    }


    SECTION("Test deserializing") {
        SequenceContainer sequences = SequenceContainer();
        sequences.setSize(3, {{20, 'R'}}); // read-structure: 20R
        sequences.extendSequences(nucleotides);

        // the copy is needed because the data of the original 'sequences' is cleared
        SequenceContainer sequenceCopy = sequences;

        // serialize
        auto serializingResult = std::vector<char>(sequences.serializableSize());
        sequences.serialize(serializingResult.data());

        // deserialize
        SequenceContainer newSequences = SequenceContainer();
        newSequences.setSize(3, {{20, 'R'}}); // read-structure: 20R
        newSequences.deserialize(serializingResult.data());

        for (int i = 0; i < 3; i++) {
            REQUIRE(newSequences.getSequence(i) == sequenceCopy.getSequence(i));
        }
    }
}
