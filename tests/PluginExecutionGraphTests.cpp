#include <iostream>
#include <regex>

#include "./catch2/catch.hpp"
#include "../framework/Plugin.h"
#include "../framework/Framework.h"
#include <boost/graph/graphviz.hpp>

using namespace std;

typedef boost::graph_traits<PluginExecutionGraph>::vertex_descriptor Vertex;
typedef boost::graph_traits<PluginExecutionGraph>::edge_descriptor Edge;
typedef boost::graph_traits<PluginExecutionGraph>::vertex_iterator VertexIterator;

class PluginExecutionGraphAccessHelper {
    PluginExecutionGraph &graph;

public:
    explicit PluginExecutionGraphAccessHelper(PluginExecutionGraph &g) : graph(g) {};

    std::map<Edge, std::vector<PluginSpecification::FragmentSpecification>> getEdgeFragmentSpecifications() {
        return this->graph.edgeFragmentSpecifications;
    }

    Vertex addVertex(Plugin *plugin) {
        return this->graph.addVertex(plugin);
    }

    Edge addEdge(Vertex sourceVertex, Vertex targetVertex,
                 PluginSpecification::FragmentSpecification fragmentSpecification) {
        return this->graph.addEdge(sourceVertex, targetVertex, move(fragmentSpecification));
    }

    Plugin *toPlugin(Vertex vertex) {
        return this->graph.toPlugin(vertex);
    }

    vector<PluginSpecification::FragmentSpecification> toFragmentSpecifications(Edge edge) {
        return this->graph.toFragmentSpecifications(edge);
    }
};

class LoggingFragment : public Fragment {
public:
    // This creates a memory leak but I think its okay because it's only in the test
    explicit LoggingFragment(string name) : Fragment(name, new SerializableFactory()) {}

    ~LoggingFragment() {
        cout << "Deleted fragment" << endl;
    }
};

class ConcretePlugin : public Plugin {
public:
    explicit ConcretePlugin(PluginSpecification *specification) {
        this->specification = specification;
    }

    void setConfig() override {};

    void init() override {};

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override {
        inputFragments->add(make_shared<LoggingFragment>("UNUSED"));
        return inputFragments;
    };

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override {
        cout << this->specification->pluginPath;
        return inputFragments;
    };

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override {
        return inputFragments;
    };

    void finalize() override {};
};

class GettingPlugin : public ConcretePlugin {
public:
    explicit GettingPlugin(PluginSpecification *specification) : ConcretePlugin(specification) {}

    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override {
        if (inputFragments->has("UNUSED")) cout << "Fragment arrived" << endl;

        return inputFragments;
    };
};

TEST_CASE("PluginExecutionGraphTests") {
    // Capture cout
    streambuf *defaultOutBuffer = cout.rdbuf();
    ostringstream capturedOutBuffer;
    cout.rdbuf(capturedOutBuffer.rdbuf());
    capturedOutBuffer.str("");

    SECTION("Should absolve n:1 mapping from Vertex to Plugin pointer successfully") {
        PluginExecutionGraph graph;
        PluginExecutionGraphAccessHelper helper(graph);

        auto plugin = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});
        Vertex vertex = helper.addVertex(plugin);

        REQUIRE(plugin == helper.toPlugin(vertex));
    }

    SECTION("Should add edge to graph and alter edgeFragmentSpecifications map") {
        PluginExecutionGraph graph;
        PluginExecutionGraphAccessHelper helper(graph);

        auto plugin1 = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});
        auto plugin2 = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});

        Vertex vertex1 = helper.addVertex(plugin1);
        Vertex vertex2 = helper.addVertex(plugin2);

        PluginSpecification::FragmentSpecification fragmentSpecification{"name", "TYPE", false};
        Edge edge = helper.addEdge(vertex1, vertex2, fragmentSpecification);

        REQUIRE(num_edges(graph) == 1);
        REQUIRE(edge.m_source == vertex1);
        REQUIRE(edge.m_target == vertex2);

        REQUIRE(helper.getEdgeFragmentSpecifications().size() == 1);
        auto fragmentSpecifications = helper.getEdgeFragmentSpecifications()[edge];

        bool fragmentFound = false;
        for (auto iterator = fragmentSpecifications.begin(); iterator != fragmentSpecifications.end(); iterator++) {
            if (iterator->fragmentName == "name" && iterator->fragmentType == "TYPE") fragmentFound = true;
        }
        REQUIRE(fragmentFound);

        delete plugin1->specification;
        delete plugin1;
        delete plugin2->specification;
        delete plugin2;
    }

    SECTION("Should add plugins successfully") {
        PluginExecutionGraph graph;
        PluginExecutionGraphAccessHelper helper(graph);

        auto plugin1 = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});
        auto plugin2 = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});

        set<Plugin *> plugins;
        plugins.insert(plugin1);
        plugins.insert(plugin2);
        graph.addPlugins(plugins);

        pair<VertexIterator, VertexIterator> vertexPair;
        set<Plugin *> verticesFromGraph;

        for (vertexPair = boost::vertices(graph); vertexPair.first != vertexPair.second; ++vertexPair.first) {
            verticesFromGraph.insert(helper.toPlugin(*vertexPair.first));
        }

        set<Plugin *>::iterator it1 = verticesFromGraph.find(plugin1);
        REQUIRE(it1 != verticesFromGraph.end());

        set<Plugin *>::iterator it2 = verticesFromGraph.find(plugin2);
        REQUIRE(it2 != verticesFromGraph.end());

        delete plugin1->specification;
        delete plugin1;
        delete plugin2->specification;
        delete plugin2;
    }

    SECTION("Should execute plugins in the right order, considering fragments arriving in the next cycle") {
        PluginExecutionGraph graph;

        set<Plugin *> plugins = {
                new ConcretePlugin(new PluginSpecification{"A", "", {},
                                                                    {{"cycle", {{"a", "aFragment"}}}}}),
                new ConcretePlugin(new PluginSpecification{"B", "", {{"cycle", {{"a", "aFragment", false}, {"d", "dFragment", false}}}},
                                                                    {{"cycle", {{"b", "bFragment"}}}}}),
                new ConcretePlugin(new PluginSpecification{"C", "", {{"cycle", {{"b", "bFragment", true}}}},
                                                                    {{"cycle", {{"c", "cFragment"}}}}}),
                new ConcretePlugin(new PluginSpecification{"D", "", {{"cycle", {{"c", "cFragment", false}}}},
                                                                    {{"cycle", {{"d", "dFragment"}}}}}),
                new ConcretePlugin(new PluginSpecification{"E", "", {{"cycle", {{"a", "aFragment", false}}}},
                                                                    {}}),
        };

        graph.addPlugins(plugins);
        graph.runCycle();

        REQUIRE(regex_match(capturedOutBuffer.str(), regex("(.*)A(.*)B(.*)")));
        REQUIRE(regex_match(capturedOutBuffer.str(), regex("(.*)A(.*)E(.*)")));
        REQUIRE(regex_match(capturedOutBuffer.str(), regex("(.*)D(.*)B(.*)")));
        REQUIRE(regex_match(capturedOutBuffer.str(), regex("(.*)C(.*)D(.*)")));
        REQUIRE(regex_match(capturedOutBuffer.str(), regex("(.*)C(.*)B(.*)")));

        for (auto plugin : plugins) {
            delete plugin->specification;
            delete plugin;
        }
        plugins.clear();
    }

    SECTION("Should delete fragment if it is not needed") {
        PluginExecutionGraph graph;

        auto plugin = new ConcretePlugin(new PluginSpecification{"", "", {}, {}});

        set<Plugin *> plugins;
        plugins.insert(plugin);
        graph.addPlugins(plugins);

        // returns a fragment that isn't needed by any plugin
        graph.runPreprocessing();
        REQUIRE(capturedOutBuffer.str() == "Deleted fragment\n");

        delete plugin->specification;
        delete plugin;
    }

    SECTION("Should delete fragment if it is not needed anymore") {
        PluginExecutionGraph graph;

        auto plugin1 = new ConcretePlugin(new PluginSpecification{"", "", {}, {{"preprocessing", {
                {"unusedFragment", "UNUSED", false}}}}});
        auto plugin2 = new GettingPlugin(new PluginSpecification{"", "", {{"preprocessing", {
                {"unusedFragment", "UNUSED", false}}}}, {}});

        set<Plugin *> plugins;
        plugins.insert(plugin1);
        plugins.insert(plugin2);
        graph.addPlugins(plugins);

        graph.runPreprocessing();
        REQUIRE(capturedOutBuffer.str() == "Fragment arrived\n" "Deleted fragment\n");

        delete plugin1->specification;
        delete plugin1;
        delete plugin2->specification;
        delete plugin2;
    }

    // TODO: Tests for error handling

    // Reset cout
    cout.rdbuf(defaultOutBuffer);
}