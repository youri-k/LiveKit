from pyplugin import *

class Example(PyPlugin):
    framework = None
    permanentFragment = None

    def setFramework(self, framework):
        self.framework = framework

    def init(self):
        self.permanentFragment = Fragment("permanent")
        self.permanentFragment.isSerialized = False
        self.permanentFragment.set("lastLetter", 0)

    def runCycle(self, inputFragments):
        rightFragment = next(x for x in inputFragments if x.name == "communication")
        currentValue = rightFragment.getInt("val")
        print(chr(ord('A') + currentValue % 26))
