#include "SerializableFactory.h"
#include "SerializableIndex.h"
#include "SerializableVector.h"
#include "SerializableSequenceElements.h"

/*
 * blueprint for adding new Serializable%s:

    {"name", [](unsigned long size, char *serializedSpace) {
        return new SerializableName(size, serializedSpace);
    }}
 */

Serializable *
SerializableFactory::createSerializable(const std::string &name, unsigned long size, char *serializedSpace) {
    return SerializableFactory::knownSerializables.at(name)(size, serializedSpace);
}

const std::map<std::string, std::function<Serializable *(unsigned long,
                                                         char *)>> SerializableFactory::knownSerializables{
        {"index",  [](unsigned long size, char *serializedSpace) {
            return new SerializableIndex(size, serializedSpace);
        }},
        {"vector", [](unsigned long size, char *serializedSpace) {
            return SerializableVectorTypeLess::createSerializableVector(size, serializedSpace);
        }},
        {"seqEl",  [](unsigned long size, char *serializedSpace) {
            (void) size;
            return new SerializableSequenceElements(serializedSpace);
        }}
};

void SerializableFactory::deleteSerializable(Serializable *serializable) {
    delete serializable;
}

extern "C" SerializableFactory *create() {
    return new SerializableFactory;
}

extern "C" void destroy(SerializableFactory *factory) {
    delete factory;
}