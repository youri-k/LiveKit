#include "FileDetector.h"
#include "utils.h"

#ifdef __linux__
#include <sys/inotify.h>
#endif

#include <boost/filesystem.hpp>
#include <cstddef>
#include <iostream>

#define MAX_EVENTS 1024 /*Max. number of events to process at one go*/
#define LEN_NAME 16 /*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  (sizeof(struct inotify_event) ) /*size of one event*/
#define BUF_LEN (MAX_EVENTS * (EVENT_SIZE + LEN_NAME)) /*buffer to store the data of events*/

FileDetector::FileDetector(std::string path) : rootPath(path) {}

void FileDetector::checkForDirectory(std::string path) {
#ifdef __APPLE__

    while(!boost::filesystem::exists(path)) {
        usleep(100000); //100 ms
    }

    return;

#elif __linux__

    //Get root directory of the wanted directory
    std::size_t found = path.find_last_of('/');
    std::string rootPath = path.substr(0, found);

    //Get directory name without full path
    std::string fileName = path.substr(found + 1);

    //Set up inotify for root directory
    int fileDescriptor = inotify_init();

    if (fileDescriptor < 0) {
        std::cout << "Couldn't add watch to path" << std::endl;
        perror("Could not initialize inotify");
    }

    int watchDescriptor = inotify_add_watch(fileDescriptor, rootPath.c_str(), IN_CREATE);

    if (watchDescriptor == -1) {
        std::cout << "Couldn't add watch to path" << std::endl;
        perror("inotify_add_watch");
    }

    char buffer[BUF_LEN];
    struct inotify_event *event;

    if (boost::filesystem::exists(path)) {
        inotify_rm_watch(fileDescriptor, watchDescriptor);
        close(fileDescriptor);
        return;
    }

    std::cout << "Waiting for " << path << " ..." << std::endl;

    while (true) {
        int i = 0;

        //Execution stops here, until the searched directory has been created
        int length = read(fileDescriptor, buffer, BUF_LEN);

        while (i < length) {
            event = (struct inotify_event *) &buffer[i];

            if (event->len) {

                bool directoryFound =
                        (event->mask & IN_CREATE) &&
                        (event->mask & IN_ISDIR) &&
                        (!strcmp(&(event->name)[0], fileName.c_str()));

                if (directoryFound) {
                    std::cout << "Directory " << std::string(event->name) << " found" << std::endl;

                    inotify_rm_watch(fileDescriptor, watchDescriptor);
                    close(fileDescriptor);

                    return;
                }

                i += sizeof(struct inotify_event) + event->len;
            }
        }
    }

#endif
}

void FileDetector::checkForAllLanes(int lanes, std::string path) {
    for (int lane = 1; lane <= lanes; lane++) {
        //std::cout << "HERE: " << this->rootPath << "/L" << toNDigits(lane, 3, '0') << path << std::endl;
        this->checkForDirectory(this->rootPath + "/L" + toNDigits(lane, 3) + path);
    }
}

void FileDetector::checkForCycleFiles(int lanes, int cycle, bool isBgzfParser) {
    if (isBgzfParser) {
        this->checkForAllLanes(lanes, "/" + toNDigits(cycle, 4) + ".bcl.bgzf");
        this->checkForAllLanes(lanes, "/" + toNDigits(cycle, 4) + ".bcl.bgzf.bci");
    } else {
        this->checkForAllLanes(lanes, "/C" + std::to_string(cycle) + ".1");
    }
}
