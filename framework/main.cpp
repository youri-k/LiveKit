#include "main.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include "Framework.h"

using namespace std;

int main(int argc, char **argv) {

    // Create a temp directory for all output files
    boost::filesystem::create_directory(boost::filesystem::path("./temp"));

    string frameworkManifestFilePath = "../config/framework.json";

    if (argc == 2) frameworkManifestFilePath = argv[1];

    cout << "Loading framework with manifest file: '" << frameworkManifestFilePath << "'" << endl;

    Framework framework(frameworkManifestFilePath);

    framework.runAllCycles();

    //framework.serializeAllFragments();
    //framework.deserializeAllFragments();
    //framework.serializeFragment("FMIndex");

    return 0;
}