#ifndef LIVEKIT_SEQUENCEREPRESENTATION_H
#define LIVEKIT_SEQUENCEREPRESENTATION_H

#include <vector>
#include <string>
#include <algorithm>

#include "../../Serializable.h"

/**
 * Container for all bases and qualities of a single read.
 * This `Read` gets extended each cycle.
 */
typedef std::vector<uint8_t> Read;

class Sequence {
private:
    /**
     * The sequence contains multiple reads (mates/barcodes) to represent e.g. 20R 8B 8B 20R
     */
    std::vector<Read> sequence;

    /**
     * maxReadLengths contains the max length to each read
     * (this is often different to the actual size of the read, because the sequence gets extended over time)
     */
    std::vector<unsigned long> maxReadLengths;
public:

    /**
     * @param readStructure is needed to initialize sequence and maxReadLengths
     */
    explicit Sequence(std::vector<std::pair<int, char>> readStructure);

    /**
     * Create a Sequence based on the state (serialized data) of a serialized Sequence
     * This is needed because for serialization and deserialization of the SequenceContainer
     * @param serializedSpace is a pointer to the serialized Data
     */
    explicit Sequence(char *serializedSpace);

    Read operator[](int i) const;

    Read &operator[](int i);

    bool operator==(Sequence s) const;

    /**
     * @param i
     * @return the pointer to the Read specified by the parameter i
     */
    virtual Read *getAddressAt(int i);

    /**
     * Returns the number of segments of the sequence
     * (e.g if the read-structure is 20R 8B 8B 20R, the function would return 4)
     * @return size of sequence
     */
    uint16_t numSegments();

    /**
     * @param index
     * @return the max length of the read specified by i
     */
    unsigned long getMaxReadLength(int index);

    /**
     *
     * @param reads The read identification which shall be concatenated
     * @return the concatenated read
     */
    virtual Read getConcatenatedSequence(const std::vector<std::pair<int, char>> &reads);

    void serialize(char *serializedSpace);

    void deserialize(char *serializedSpace);

    unsigned long serializableSize();

    virtual ~Sequence() = default;
};

/**
 * A `SequenceContainer` object holds all reads for a single tile and exposes interaction possibilities as well as a serialize interface.
 */
class SequenceContainer {
    // TODO: Inherit from Serializable - Interface
private:
    std::vector<Sequence> sequences;

    void extendSequence(unsigned long sequenceNumber, char nucleotides);

public:
    SequenceContainer() : sequences(std::vector<Sequence>()) {}

    /**
     * Calls extendSequence for each sequence in sequences with the associated nucleotide
     * @param nucleotide is a vector<char> where each char is a new nucleotide for this specific tile. (nucleotide.size() == sequences.size())
     */
    void extendSequences(const std::vector<char> &nucleotide);

    void serialize(char *serializedSpace);

    void deserialize(char *serializedSpace);

    /**
     * @return the vector of sequences for this tile
     */
    virtual const std::vector<Sequence> &getAllSequences();

    /**
     * Return a specific sequence from this tile
     * @param index specifies the position of the sequence in the vector
     * @return
     */
    virtual Sequence &getSequence(int index);

    /**
     * Sets the size of the vector which holds the sequences and initialize each sequence
     * @param size specifies how many sequences this tile has
     * @param readStructure is needed to initialize Sequence
     */
    void setSize(int size, std::vector<std::pair<int, char>> readStructure);

    unsigned long serializableSize();

    virtual ~SequenceContainer() = default;
};

#endif //LIVEKIT_SEQUENCEREPRESENTATION_H
