#include <cstring>
#include <fstream>
#include "SequenceContainer.h"

using namespace std;

#define revtwobit_repr(n) ((n) == 0 ? 'A' : \
                           (n) == 1 ? 'C' : \
                           (n) == 2 ? 'G' : 'T')


Sequence::Sequence(vector<pair<int, char>> readSizes) {
    for (auto &read : readSizes) {
        this->sequence.emplace_back();
        this->maxReadLengths.push_back(read.first);
    }
}

Sequence::Sequence(char *serializedSpace) {
    this->deserialize(serializedSpace);
}

Read Sequence::operator[](int i) const {
    return sequence[i];
}

Read &Sequence::operator[](int i) {
    return sequence[i];
}

Read *Sequence::getAddressAt(int i) {
    return &sequence[i];
}

uint16_t Sequence::numSegments() {
    return (uint16_t) this->sequence.size();
}

unsigned long Sequence::getMaxReadLength(int index) {
    return this->maxReadLengths[index];
}

void Sequence::serialize(char *serializedSpace) {
    unsigned long size = this->serializableSize();
    SERIALIZE_VALUE(size);

    unsigned long numReadElements = this->sequence.size();
    SERIALIZE_VALUE(numReadElements);

    // serialize readLength (the max. length of the reads)
    for (auto &readLength : this->maxReadLengths) {
        memcpy(serializedSpace, &readLength, sizeof(unsigned long));
        serializedSpace += sizeof(unsigned long);
    }

    // serialize the current length of the reads
    for (auto &read : this->sequence) {
        auto currentSize = read.size();
        memcpy(serializedSpace, &currentSize, sizeof(unsigned long));
        serializedSpace += sizeof(unsigned long);
    }

    // serialize the data of the reads
    for (auto &read : this->sequence) {
        memcpy(serializedSpace, read.data(), read.size());
        serializedSpace += read.size();
    }

    this->maxReadLengths.clear();
    this->sequence.clear();
}

unsigned long Sequence::serializableSize() {
    unsigned long size = 0;
    size += sizeof(unsigned long); // for storing the total size
    size += sizeof(unsigned long); // for storing the number of readElements
    size += this->maxReadLengths.size() * sizeof(unsigned long);
    size += this->sequence.size() * sizeof(unsigned long);

    for (auto &read : this->sequence) size += read.size();

    return size;
}

void Sequence::deserialize(char *serializedSpace) {
    // make sure that old data is removed
    this->sequence.clear();
    this->maxReadLengths.clear();

    unsigned long size;
    DESERIALIZE_VALUE(size);

    unsigned long numReadElements;
    DESERIALIZE_VALUE(numReadElements);

    // deserialize readLength (the max. length of the reads)
    for (unsigned int i = 0; i < numReadElements; i++) {
        unsigned long tempMaxLength;
        memcpy(&tempMaxLength, serializedSpace, sizeof(unsigned long));
        serializedSpace += sizeof(unsigned long);
        this->maxReadLengths.push_back(tempMaxLength);
    }

    // deserialize the current length of the reads
    vector<unsigned long> tempLengths;
    for (unsigned int i = 0; i < numReadElements; i++) {
        unsigned long tempLength;
        memcpy(&tempLength, serializedSpace, sizeof(unsigned long));
        serializedSpace += sizeof(unsigned long);
        tempLengths.push_back(tempLength);
    }

    // deserialize the data of the reads
    for (unsigned int i = 0; i < numReadElements; i++) {
        unsigned long readLength = tempLengths[i];
        Read tempRead = Read(readLength);
        memcpy(tempRead.data(), serializedSpace, readLength);
        serializedSpace += readLength;
        this->sequence.push_back(tempRead);
    }
}

bool Sequence::operator==(Sequence s) const {
    if(this->sequence.size() != s.sequence.size()) return false;

    for(uint64_t i=0; i < this->sequence.size(); i++){
        if (this->sequence[i] != s.sequence[i]) return false;
        if (this->maxReadLengths[i] != s.maxReadLengths[i]) return false;
    }

    return true;
}

Read Sequence::getConcatenatedSequence(const vector<pair<int, char>>& reads) {
    vector<uint8_t> concatenatedSequence;

    for (int i = 0; i < (int)reads.size(); i++) {
        concatenatedSequence.insert(concatenatedSequence.end(), this->sequence[i].begin(), this->sequence[i].end());
    }

    return concatenatedSequence;
}

void SequenceContainer::serialize(char *serializedSpace) {
    unsigned long size = this->serializableSize();
    unsigned long bufferPosition = 0;

    memcpy(serializedSpace, &size, sizeof(unsigned long));
    bufferPosition += sizeof(unsigned long);

    // serialize each sequence
    for(auto &sequence: this->sequences){
        // we need to store the size here, because the data could be cleared after it is serialized
        unsigned long sequenceSize = sequence.serializableSize();
        sequence.serialize(serializedSpace + bufferPosition);
        bufferPosition += sequenceSize;
    }

    this->sequences.clear();
}

void SequenceContainer::deserialize(char *serializedSpace) {
    // clear old data
    this->sequences.clear();

    unsigned long size = 0;
    unsigned long bufferPosition = 0;

    memcpy(&size, serializedSpace, sizeof(unsigned long));
    bufferPosition += sizeof(unsigned long);

    while(bufferPosition < size){
        unsigned long sequenceSize = 0;
        memcpy(&sequenceSize, serializedSpace + bufferPosition, sizeof(unsigned long));

        auto tempSequence = Sequence(serializedSpace + bufferPosition);
        this->sequences.push_back(tempSequence);

        bufferPosition += sequenceSize;
    }
}

void SequenceContainer::extendSequence(unsigned long sequenceNumber, char nucleotide) {
    auto &sequence = this->sequences[sequenceNumber];
    for(int i = 0; i < sequence.numSegments(); i++){
        if (sequence[i].size() < sequence.getMaxReadLength(i)){
            sequence[i].push_back(nucleotide);
            return;
        }
    }
}

void SequenceContainer::extendSequences(const vector<char> &nucleotides) {
    for (unsigned long i = 0; i < nucleotides.size(); i++) {
        extendSequence(i, nucleotides[i]);
    }
}

unsigned long SequenceContainer::serializableSize() {
    unsigned long size = 0;
    size += sizeof(unsigned long); // for storing the totalLength at the beginning

    for (auto &sequence : this->sequences) // this->sequences.size() == number of sequences per tile
        size += sequence.serializableSize();

    return size;
}

const vector<Sequence> &SequenceContainer::getAllSequences() {
    return this->sequences;
}

Sequence &SequenceContainer::getSequence(int index) {
    return this->sequences[index];
}

void SequenceContainer::setSize(int size, vector<pair<int, char>> readStructure) {
    this->sequences.resize(size, Sequence(readStructure));
}