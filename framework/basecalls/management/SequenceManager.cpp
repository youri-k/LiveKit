#include <fstream>
#include <iostream>
#include "SequenceManager.h"

using namespace std;

SequenceManager::SequenceManager(std::vector<uint16_t> lanes, std::vector<uint16_t> tiles) {
    for (auto lane : lanes) {
        this->sequenceRepresentations[lane] =  map<uint16_t, SequenceContainer>();
        for (auto tile : tiles) {
            this->sequenceRepresentations[lane][tile] = SequenceContainer();
        }
    }
}


void SequenceManager::writeAllSequencesToDisk(int cycle) {
    ofstream outputFile;
    outputFile.open("temp/example" + to_string(cycle) + ".txt");
    for (auto &laneEntry : this->sequenceRepresentations) {
        for (auto &tileEntry : laneEntry.second) {
            auto size = tileEntry.second.serializableSize();
            char *serializedReads = (char *) malloc(size);
            tileEntry.second.serialize(serializedReads);
            outputFile.write(serializedReads, size);
            free(serializedReads);
        }
    }

    outputFile.close();
}

SequenceContainer &SequenceManager::getSequences(uint16_t lane, uint16_t tile) {
    return this->sequenceRepresentations[lane][tile];
}
