#include <stdlib.h>

#include "PluginExecutionGraph.h"
#include "../fragments/FragmentContainer.h"

using namespace std;

Plugin *PluginExecutionGraph::toPlugin(PluginExecutionGraph::Vertex vertex) {
    return (*this)[vertex];
}

vector<PluginSpecification::FragmentSpecification>
PluginExecutionGraph::toFragmentSpecifications(PluginExecutionGraph::Edge edge) {
    return this->edgeFragmentSpecifications[edge];
}

bool PluginExecutionGraph::allEdgesProcessed(PluginExecutionGraph::Vertex pluginVertex) {
    for (Edge edge : this->invertedAdjacencyList[pluginVertex]) {
        if (!this->isProcessed[edge])
            return false;
    }
    return true;
}

pair<PluginExecutionGraph::Edge, bool> PluginExecutionGraph::getEdge(PluginExecutionGraph::Vertex sourceVertex,
                                                                     PluginExecutionGraph::Vertex targetVertex) {
    return boost::edge(sourceVertex, targetVertex, *this);
}

void PluginExecutionGraph::setupInvertedAdjacencyList() {
    pair<EdgeIterator, EdgeIterator> edgePair;
    for (edgePair = boost::edges(*this); edgePair.first != edgePair.second; ++edgePair.first) {
        Vertex targetPluginVertex = target(*edgePair.first, *this);
        this->invertedAdjacencyList.at(targetPluginVertex).push_back(this->toEdge(edgePair));
    }
}

void
PluginExecutionGraph::updateExecutionQueue(Vertex currentPluginVertex, FragmentContainer *outputFragmentContainer) {
    pair<OutEdgeIterator, OutEdgeIterator> outEdgePair;
    for (outEdgePair = boost::out_edges(currentPluginVertex, *this);
         outEdgePair.first != outEdgePair.second; ++outEdgePair.first) {
        Vertex targetPluginVertex = target(*outEdgePair.first, *this);

        for (auto const &fragmentSpecification : this->toFragmentSpecifications(this->toEdge(outEdgePair))) {
            string fragmentType = fragmentSpecification.fragmentType;

            if (!fragmentType.empty() && outputFragmentContainer->has(fragmentType)) {
                // Move required fragments over to the input containers of the following plugins
                this->preparedInputContainer[targetPluginVertex]->add(outputFragmentContainer->get(fragmentType));
            }
            this->isProcessed[this->toEdge(outEdgePair)] = true;
        }

        if (this->allEdgesProcessed(targetPluginVertex)) {
            this->executionQueue.push_back(targetPluginVertex);
        }
    }
}

void PluginExecutionGraph::addOutputFragmentSpecificationToPluginMapping(
        PluginSpecification::FragmentSpecification outputFragmentSpecification, Vertex vertex,
        string inputEdgeCategory) {
    // Insert if not yet present
    auto insertionResult = this->outputFragmentSpecificationToPlugin.insert(make_pair(
            outputFragmentSpecification.fragmentName,
            OutputEdgeEntry{outputFragmentSpecification.fragmentType, vertex, move(inputEdgeCategory)}
    ));

    bool wasInserted = insertionResult.second;

    if (!wasInserted) {
        auto retrievedKeyValuePair = insertionResult.first;
        auto retrievedFragmentType = retrievedKeyValuePair->second.type;
        auto retrievedVertex = retrievedKeyValuePair->second.vertex;

        if (outputFragmentSpecification.fragmentType == retrievedFragmentType) {
            // Name and type matched previously with another output fragment specification
            string message = "Output fragment '" + outputFragmentSpecification.fragmentName
                             + "' of type '" + outputFragmentSpecification.fragmentType
                             + "' was already specified in plugins '" + this->toPlugin(vertex)->specification->getDisplayName()
                             + "' and '" + this->toPlugin(retrievedVertex)->specification->getDisplayName()
                             + "'";
            throw runtime_error(message);
        } else {
            // Name matches with another output fragment specification, but type differs
            string message = "Output fragment '" + outputFragmentSpecification.fragmentName
                             + "' was specified in plugin '" + this->toPlugin(vertex)->specification->getDisplayName()
                             + "' with type '" + outputFragmentSpecification.fragmentType
                             + "' and plugin '" + this->toPlugin(retrievedVertex)->specification->getDisplayName()
                             + "' with type '" + retrievedFragmentType
                             + "'";
            throw runtime_error(message);
        }
    }
}

PluginExecutionGraph::Vertex PluginExecutionGraph::addVertex(Plugin *plugin) {
    return boost::add_vertex(plugin, *this);
}

PluginExecutionGraph::Edge PluginExecutionGraph::addEdge(Vertex sourceVertex, Vertex targetVertex,
                                                         PluginSpecification::FragmentSpecification fragmentSpecification) {
    Edge edge;
    bool isInserted, alreadyExists;
    boost::tie(edge, alreadyExists) = boost::edge(sourceVertex, targetVertex, *this);

    if (alreadyExists) {
        this->edgeFragmentSpecifications[edge].push_back(fragmentSpecification);
    } else {
        tie(edge, isInserted) = boost::add_edge(sourceVertex, targetVertex, *this);

        if (!isInserted) {
            string message = "Could not insert fragment-edge between plugin-vertices '"
                             + this->toPlugin(sourceVertex)->specification->getDisplayName()
                             + "' and '" + this->toPlugin(targetVertex)->specification->getDisplayName() + "'";
            throw runtime_error(message);
        }
        this->edgeFragmentSpecifications[edge] = {fragmentSpecification};
    }

    return edge;
}

void PluginExecutionGraph::addInputEdgesForPlugin(Vertex targetVertex, Vertex startPluginVertex, Vertex endPluginVertex,
                                                  string inputEdgeCategory) {
    auto targetPlugin = this->toPlugin(targetVertex);

    for (auto &inputSpecification : targetPlugin->specification->inputFragmentSpecifications[inputEdgeCategory]) {

        // Find fragment by name
        map<string, OutputEdgeEntry>::iterator mapKeyValuePair = this->outputFragmentSpecificationToPlugin.find(inputSpecification.fragmentName);

        string outputFragmentType = mapKeyValuePair->second.type;
        Vertex sourceVertex = mapKeyValuePair->second.vertex;
        Plugin *sourcePlugin = this->toPlugin(sourceVertex);
        string inputEdgeCategoryOfOutputtingVertex = mapKeyValuePair->second.inputEdgeCategory;
        bool fragmentNotFound = (mapKeyValuePair == this->outputFragmentSpecificationToPlugin.end());

        if (fragmentNotFound) {
            string message = "Plugin '" + targetPlugin->specification->getDisplayName()
                             + "' is not provided with an output-fragment '" + inputSpecification.fragmentName
                             + "' of type '" + inputSpecification.fragmentType
                             + "'";
            throw runtime_error(message);
        }

        if (inputSpecification.fragmentType != outputFragmentType) {
            string message = "Fragment '" + inputSpecification.fragmentName
                             + "' was specified as output of plugin '" + sourcePlugin->specification->getDisplayName()
                             + "' with type '" + outputFragmentType
                             + "' and input of plugin '" + targetPlugin->specification->getDisplayName()
                             + "' with mismatching type '" + outputFragmentType
                             + "'";
            throw runtime_error(message);
        }

        // Identify fragments that we want from the previous cycle
        if (inputSpecification.fromPrecedingCycle) {
            // 1. Draw an edge from the start plugin to the plugin that needs the fragment (target plugin)
            this->addEdge(startPluginVertex, targetVertex, inputSpecification);
            // 2. Draw an edge from the plugin (source plugin) that produces the fragment (in the previous cycle) to the end plugin
            this->addEdge(sourceVertex, endPluginVertex, inputSpecification);
        } else {
            if (inputEdgeCategoryOfOutputtingVertex != inputEdgeCategory) {
                // Dependency from a plugin of a previous hook (e.g. a runCycle-hook needs a fragment from preprocessing)

                // 1. Draw an edge from the source plugin to the start plugin of the current hook
                this->addEdge(sourceVertex, startPluginVertex, inputSpecification);

                // 2. Draw an edge from the current start plugin to the target plugin
                this->addEdge(startPluginVertex, targetVertex, inputSpecification);

                // 3. Draw edges between current start plugin and end plugin (both directions)
                //    (to keep the Fragment alive in case of multiple execution)
                this->addEdge(startPluginVertex, endPluginVertex, inputSpecification);

            } else {
                // Standard scenario: Dependency from one plugin to another inside one hook/cycle
                this->addEdge(sourceVertex, targetVertex, inputSpecification);
            }
        }
    }
}

void PluginExecutionGraph::runPreprocessing() {
    this->executePlugins(&Plugin::runPreprocessing, this->helperPluginVertices[0], this->helperPluginVertices[1]);
}

void PluginExecutionGraph::runCycle() {
    this->executePlugins(&Plugin::runCycle, this->helperPluginVertices[2], this->helperPluginVertices[3]);
}

void PluginExecutionGraph::runFullReadPostprocessing() {
    this->executePlugins(&Plugin::runFullReadPostprocessing, this->helperPluginVertices[4],
                         this->helperPluginVertices[5]);
}

// TODO: Cashing execution order from each of the three run-methods
//       (at the moment the order is recomputed every time one of these methods is called)
void
PluginExecutionGraph::executePlugins(FragmentContainer *(Plugin::*hook)(FragmentContainer *), Vertex startPluginVertex,
                                     Vertex endPluginVertex) {
    // Clearing queue to ensure that there is just one start plugin instance
    this->executionQueue.clear();
    this->executionQueue.push_back(startPluginVertex);

    try {
        while (!this->executionQueue.empty()) {
            Vertex currentPluginVertex = this->executionQueue.front();
            this->executionQueue.pop_front();

            auto outputFragmentContainer = (this->toPlugin(currentPluginVertex)->*hook)(
                    this->preparedInputContainer[currentPluginVertex]);

            this->updateExecutionQueue(currentPluginVertex, outputFragmentContainer);

            if (currentPluginVertex == endPluginVertex) {
                for (auto const &fragment : outputFragmentContainer->fragments)
                    this->preparedInputContainer[startPluginVertex]->add(fragment.second);

                this->preparedInputContainer[currentPluginVertex]->clear();

                // Mark all edges as unprocessed
                pair<EdgeIterator, EdgeIterator> edgePair;
                for (edgePair = boost::edges(*this); edgePair.first != edgePair.second; ++edgePair.first) {
                    this->isProcessed[this->toEdge(edgePair)] = false;
                }
                return;
            }

            // Clear preparedInputContainer of last executed plugin - so it can be refilled in the next cycle
            this->preparedInputContainer[currentPluginVertex]->clear();
        }
    } catch (exception &e) {
        cerr << "Could not process execution queue: " << e.what() << endl;
    }
}

void PluginExecutionGraph::setupSubGraph(Vertex startPluginVertex, Vertex endPluginVertex, string edgeCategory,
                                         set<Plugin *> plugins) {
    // Create vertices
    vector<Vertex> vertices;

    for (auto const &plugin : plugins) {
        vertices.push_back(this->addVertex(plugin));

        // Initialize instance variables that are dependent on the plugin number
        this->invertedAdjacencyList.emplace_back(vector<Edge>());
        this->preparedInputContainer.push_back(new FragmentContainer());

        // Create reverse mapping for more efficient matching
        for (auto &outputFragmentSpecification : plugin->specification->outputFragmentSpecifications[edgeCategory]) {
            this->addOutputFragmentSpecificationToPluginMapping(outputFragmentSpecification, vertices.back(),
                                                                edgeCategory);
        }
    }

    // Create edges
    try {
        auto emptyFragmentSpecification = PluginSpecification::FragmentSpecification{"", "", false};
        for (auto vertex : vertices) {
            // Create edges for matching fragments (also across cycles)
            this->addInputEdgesForPlugin(vertex, startPluginVertex, endPluginVertex, edgeCategory);

            // Create an edge from the start plugin to every plugin
            this->addEdge(startPluginVertex, vertex, emptyFragmentSpecification);

            // Create an edge from every plugin to the end plugin
            this->addEdge(vertex, endPluginVertex, emptyFragmentSpecification);
        }

    } catch (exception &e) {
        cerr << "Could not add edges: " << e.what() << endl;
    }

    this->setupInvertedAdjacencyList();
}

void PluginExecutionGraph::addPlugins(set<Plugin *> plugins) {
    this->helperPlugins = {
            new HelperPlugin(new PluginSpecification("START-PRE", "")),
            new HelperPlugin(new PluginSpecification("END-PRE", "")),
            new HelperPlugin(new PluginSpecification("START-RUN", "")),
            new HelperPlugin(new PluginSpecification("END-RUN", "")),
            new HelperPlugin(new PluginSpecification("START-POST", "")),
            new HelperPlugin(new PluginSpecification("END-POST", ""))
    };

    // Initialize instance variables that are dependent on the plugin number
    for (auto plugin : this->helperPlugins) {
        this->helperPluginVertices.push_back(this->addVertex(plugin));
        this->invertedAdjacencyList.emplace_back(vector<Edge>());
        this->preparedInputContainer.push_back(new FragmentContainer());
    }

    // Connect sub graphs with additional edges to ensure that the current start plugin has only one incoming edge
    auto emptyFragmentSpecification = PluginSpecification::FragmentSpecification{"", "", false};
    this->addEdge(this->helperPluginVertices[1], this->helperPluginVertices[2], emptyFragmentSpecification);
    this->addEdge(this->helperPluginVertices[3], this->helperPluginVertices[4], emptyFragmentSpecification);

    this->setupSubGraph(this->helperPluginVertices[0], this->helperPluginVertices[1], "preprocessing", plugins);
    this->setupSubGraph(this->helperPluginVertices[2], this->helperPluginVertices[3], "cycle", plugins);
    this->setupSubGraph(this->helperPluginVertices[4], this->helperPluginVertices[5], "postprocessing", plugins);
}

PluginExecutionGraph::~PluginExecutionGraph() {
    for (auto &container : this->preparedInputContainer) delete container;
    for (auto plugin : this->helperPlugins) {
        delete plugin->specification;
        delete plugin;
    }
}
