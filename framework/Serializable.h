#ifndef LIVEKIT_SERIALIZABLE_H
#define LIVEKIT_SERIALIZABLE_H

#include <string>
#include <cstring>

#define SERIALIZE_VALUE(val) memcpy(serializedSpace, &val, sizeof(val)); serializedSpace += sizeof(val)
#define DESERIALIZE_VALUE(val) memcpy(&val, serializedSpace, sizeof(val)); serializedSpace += sizeof(val)

/**
 * Interface which should be implemented to have a uniform serializing interface
 */
class Serializable {
protected:
    /**name that identifies the Serializable. It should be overwritten in child classes.
     * used in SerializableFactory.h in the map of "knownSerializables"
     * @return the name of the Serializable
     */
    virtual const std::string name() const = 0;

    /**
     * Method to serialize 'size of the name(), name()' into the space given by serializedSpace
     * @param serializedSpace The char* to which the name will be written.
     */
    virtual void serializeName(char **serializedSpace) {
        unsigned long size = this->name().size();
        memcpy(*serializedSpace, &size, sizeof(unsigned long));
        *serializedSpace += sizeof(unsigned long);
        memcpy(*serializedSpace, this->name().c_str(), size * sizeof(char));
        *serializedSpace += size * sizeof(char);
    };

    /**
     * Method to deserialize the name
     * as the name is not further needed by the Serializable, the name is simply omitted by moving the pointer to its end.
     * @param serializedSpace The char* which holds the data.
     */
    virtual void deserializeName(char **serializedSpace) {
        *serializedSpace += this->serializableNameSize();
    };

public:
    /**
     * Method to get the size of the whole Serializable in serialized status in Bytes, consisting of (size of the name, name, size of the data, data)
     * @return Number of Bytes of the object, that can be serialized
     */
    virtual unsigned long serializableSize() {
        return this->serializableNameSize() + this->serializableDataSize();
    };

    /**
     * Method to get the size of the serialized name in Bytes
     * @return Number of Bytes used by the name
     */
    virtual unsigned long serializableNameSize() const {
        return sizeof(unsigned long) + this->name().size() * sizeof(char);
    };

    /**
     * Method to get the number of Bytes of the data in serialized status, should be overwritten in child class
     * @return Number of Bytes of your data, that can be serialized
     */
    virtual unsigned long serializableDataSize() = 0;

    /**
     * Method to serialize all data of the given object into the given space. The data will be freed afterwards.
     * after execution of the method, the given space contains: sizeof name() in Bytes, name(), serializedData (individually implemented)
     * @param serializedSpace The char* to which the serializedData should be written. This space is at least `serializableSize()` bytes large.
     */
    virtual void serialize(char *serializedSpace) {
        this->serializeName(&serializedSpace);
        this->serializeData(serializedSpace);
        this->freeData();
    };

    /**
     * Method to serialize all data of the given object into the given space. The data won't be freed afterwards.
     * @param serializedSpace The char* to which the serializedData should be written. This space is at least `serializableSize()` bytes large.
     */
    virtual void backup(char *serializedSpace) {
        this->serializeName(&serializedSpace);
        this->serializeData(serializedSpace);
    }

    /**
     * Method to serialize all of your data into the given space. The name of the Serializabe is not serialized.
     * as this method is specific for any serializable, it must be overwritten in the child class
     * @param serializedSpace The char* to which the serializedData should be written. This space is at least `serializableDataSize()` bytes large.
     */
    virtual void serializeData(char *serializedSpace) = 0;

    /**
     * Method to free all of your data to decrease memory usage
     */
    virtual void freeData() = 0;

    /**
     * Method to initialize all data (name and data) of a Serializable from a data point to which data had been serialized previously.
     * @param serializedSpace char* which holds the serialized data
     */
    virtual void deserialize(char *serializedSpace) {
        this->deserializeName(&serializedSpace);
        this->deserializeData(serializedSpace);
    };

    /**
     * Method to initialize all data from a serializable from a data point to which data had been serialized previously.
     * this data does not contain the name of the Serializable.
     * @param serializedSpace char* which holds the serialized data
     */
    virtual void deserializeData(char *serializedSpace) = 0;

    virtual ~Serializable() = default;
};

#endif //LIVEKIT_SERIALIZABLE_H
