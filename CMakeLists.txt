#######################################################################################################################
#                                                                                                                     #
#  #########            ####                                          #######    #######  ####         ####           #
#  #:::::::#           #::::#                                         #:::::#    #:::::# #::::#      ##:::#           #
#  #:::::::#            ####                                          #:::::#    #:::::#  ####       #::::##          #
#  ##:::::##                                                          #:::::#   #::::::#             #::::#           #
#  #:::#            ##############           ####### #############    #::::#  #:::::#################::::#######      #
#  #:::#            #:::::# #:::::#         #:::::###::::::::::::##   #:::# #:::::#   #:::::##::::::::::::::::#       #
#  #:::#             #::::#  #:::::#       #:::::##::::::#####:::::## #::::#:::::#     #::::##::::::::::::::::#       #
#  #:::#             #::::#   #:::::#     #:::::##::::::#     #:::::# #:::::::::#      #::::#######::::::######       #
#  #:::#             #::::#    #:::::#   #:::::# #:::::::#####::::::# #:::::::::#      #::::#      #::::#             #
#  #:::#             #::::#     #:::::# #:::::#  #:::::::::::::::::#  #::::#:::::#     #::::#      #::::#             #
#  #:::#             #::::#      #:::::#:::::#   #::::::###########   #:::# #:::::#    #::::#      #::::#             #
#  #:::#         ### #::::#       #:::::::::#    #:::::::#            #::::#  #:::::#  #::::#      #::::#    ######   #
#  ##:::#########::::##::::::#     #:::::::#       #::::::::#         #:::::#   #::::::##::::::#     #:::::####:::::# #
#  #:::::::::::::::::##::::::#      #:::::#         #::::::::######## #:::::#    #:::::##::::::#     ##:::::::::::::# #
#  #:::::::::::::::::##::::::#       #:::#           ##:::::::::::::# #:::::#    #:::::##::::::#       #:::::::::::## #
#  ###########################        ###              ############## #######    ###############        ###########   #
#                                                                                                                     #
#######################################################################################################################

cmake_minimum_required(VERSION 2.8)
project(livekit) # Defines implicitly `PROJECT_NAME`, `PROJECT_SOURCE_DIR` and `PROJECT_BINARY_DIR`

# Set environment variables
set(CMAKE_MACOSX_RPATH 1)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W -Wall -fPIC -std=gnu++14 -pthread")  # c++14 is required by Seqan
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_BUILD_TYPE Debug) # Allowed is `Release` or `Debug`

###############################################################################
########################### External Dependencies #############################
###############################################################################

################# Setup Boost libraries ###################

set(Boost_NO_SYSTEM_PATH ON)
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME ON)

include_directories(${BOOST_INCLUDE_DIR})
include_directories(${PYTHON_INCLUDE_DIRS})

if (APPLE)
    include_directories("/Library/Frameworks/Python.framework/Versions/3.6/include/python3.6m/")
else()
    include_directories("/usr/include/python3.6/")
endif()

link_directories(${BOOST_INCLUDE_DIR})
link_directories(${BOOST_LIBRARY_DIR})
unset(Boost_INCLUDE_DIR CACHE)
unset(Boost_LIBRARY_DIRS CACHE)
find_package(PythonLibs 3 REQUIRED)

if (APPLE)
    find_package(Boost COMPONENTS python37 system filesystem program_options iostreams REQUIRED)
else()
    find_package(Boost COMPONENTS python36 system filesystem program_options iostreams REQUIRED)
endif()

############## Setup Zlib and lz4 libraries ###############

find_package(ZLIB REQUIRED)
# Possibly adjust this to [pathToLz4]/lib if using downloaded lz4 source code
set(LZ4_PATH /usr/local/lib)
include_directories(${LZ4_PATH})
link_directories(${LZ4_PATH})
set(CompressionLibs "${ZLIB_LIBRARIES};lz4")

################## Setup Seqan libraries ##################

# This needs to be done AFTER searching for Zlib
set (CMAKE_MODULE_PATH "/usr/local/lib/seqan/util/cmake") # adjust this to [pathToSeqanCode]/util/cmake
set (SEQAN_INCLUDE_PATH "/usr/local/lib/seqan/include/") #a adjust this to [pathToSeqanCode]/include

# Enable features for libbz2 and Zlib
#set(SEQAN_FIND_DEPENDENCIES ZLIB BZip2) # According to Seqan tutorial
set(SEQAN_FIND_DEPENDENCIES BZip2)
find_package(SeqAn REQUIRED)

# Add include directories, defines, and flags for Seqan (and its dependencies)
include_directories(${SEQAN_INCLUDE_DIRS})
add_definitions(${SEQAN_DEFINITIONS})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${SEQAN_CXX_FLAGS}")

###############################################################################
####################### Framework, Plugins & Libraries ########################
###############################################################################

# Collect the source file paths from the framework target recursively
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/framework") # Sets `FRAMEWORK_SOURCES`

############# Prepare Build_FrameworkLibs #################

add_library(Build_FrameworkLibs ${FRAMEWORK_SOURCES})
target_compile_definitions(Build_FrameworkLibs PUBLIC -DTEST_VIRTUAL=)
target_link_libraries(Build_FrameworkLibs
        ${SEQAN_LIBRARIES}
        ${Boost_LIBRARIES}
        ${CompressionLibs}
        ${PYTHON_LIBRARIES}
        SerializableIndex
        dl)

################## Prepare HiLiveLibs ######################

# Set the version number
add_definitions(-DHiLive_VERSION_MAJOR=2)
add_definitions(-DHiLive_VERSION_MINOR=0)

set(Hilive_libraries_path "${CMAKE_CURRENT_SOURCE_DIR}/plugins/HiLive_sharedLibraries")
include_directories(Hilive_libraries_path)
add_subdirectory(${Hilive_libraries_path})
add_library(HiLiveLibs ${HILIVE_SOURCES}) # `HILIVE_SOURCES` set from contained CMakeList file

set_target_properties(HiLiveLibs PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(HiLiveLibs
        ${CompressionLibs}
        ${Boost_LIBRARIES}
        ${SEQAN_LIBRARIES}
        ${PYTHON_LIBRARIES}
        SerializableIndex)

################# Prepare PluginLibrary ###################

add_library(PluginLibrary
        ${CMAKE_CURRENT_SOURCE_DIR}/framework/configuration/Configurable.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/framework/configuration/RunInfoParser.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/framework/configuration/SampleSheetParser.cpp
        )
target_compile_definitions(PluginLibrary PUBLIC -DTEST_VIRTUAL=)
set_target_properties(PluginLibrary PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(PluginLibrary ${Boost_LIBRARIES})

if (EXISTS ../plugins/binary/)
    set(PATH_TO_BINARY ../plugins/binary/)
elseif (EXISTS ../source/plugins/binary/)
    set(PATH_TO_BINARY ../source/plugins/binary/)
endif (EXISTS ../plugins/binary/)

###########################################################
################## Setup Serializables ####################
###########################################################

############### Prepare Serializable Factory ##############
add_library(SerializableFactory SHARED serializables/SerializableFactory.cpp)
set_target_properties(SerializableFactory PROPERTIES POSITION_INDEPENDENT_CODE ON)
set_target_properties(SerializableFactory PROPERTIES SUFFIX .so)
target_link_libraries(SerializableFactory SerializableIndex SerializableSequenceElements)
add_custom_command(TARGET SerializableFactory POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:SerializableFactory> ${PATH_TO_BINARY})

############### Prepare Serializables #####################

add_library(SerializableIndex serializables/SerializableIndex.cpp)
target_link_libraries(SerializableIndex HiLiveLibs)

add_library(SerializableAlignment serializables/SerializableAlignment.cpp)
target_link_libraries(SerializableAlignment HiLiveLibs)

add_library(SerializableSequenceElements serializables/SerializableSequenceElements.cpp)
target_link_libraries(SerializableSequenceElements HiLiveLibs)


###########################################################
################# Setup Plugins & main ####################
###########################################################

################## Prepare Example Plugin #################

add_library(Example SHARED plugins/Example/Example.cpp)
set_target_properties(Example PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(Example PluginLibrary SerializableIndex)
set_target_properties(Example PROPERTIES SUFFIX .so)

# Move compiled '.so-library' to '../plugins/binary'
add_custom_command(TARGET Example POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:Example> ${PATH_TO_BINARY})

################## Prepare Bcl2FastQ Plugin ################

add_library(Bcl2FastQ SHARED plugins/Bcl2FastQ/bcl2FastQ.cpp)
set_target_properties(Bcl2FastQ PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(Bcl2FastQ PluginLibrary ${CompressionLibs} ${SEQAN_LIBRARIES} ${PYTHON_LIBRARIES})
set_target_properties(Bcl2FastQ PROPERTIES SUFFIX .so)

add_custom_command(TARGET Bcl2FastQ POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:Bcl2FastQ> ${PATH_TO_BINARY})

########### Prepare HiLive BuildIndex Plugin ##############

add_library(HiLive_BuildIndex SHARED plugins/HiLive_BuildIndex/BuildIndex.cpp)
set_target_properties(HiLive_BuildIndex PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(HiLive_BuildIndex HiLiveLibs PluginLibrary SerializableIndex)
set_target_properties(HiLive_BuildIndex PROPERTIES SUFFIX .so)

add_custom_command(TARGET HiLive_BuildIndex POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:HiLive_BuildIndex> ${PATH_TO_BINARY})

################# Prepare hilive Plugin ###################

add_library(Hilive_Alignment SHARED plugins/HiLive_Alignment/hilive.cpp)
set_target_properties(Hilive_Alignment PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(Hilive_Alignment
        PluginLibrary
        HiLiveLibs
        SerializableIndex
        SerializableAlignment
        SerializableSequenceElements)
set_target_properties(Hilive_Alignment PROPERTIES SUFFIX .so)

add_custom_command(TARGET Hilive_Alignment POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:Hilive_Alignment> ${PATH_TO_BINARY})


############# Prepare ReadPositionParser Plugin ############

add_library(ReadPositionParser SHARED plugins/ReadPositionParser/ReadPositionParser.cpp)
target_link_libraries(ReadPositionParser PluginLibrary ${CompressionLibs} ${SEQAN_LIBRARIES} SerializableIndex)
set_target_properties(ReadPositionParser PROPERTIES SUFFIX .so)

add_custom_command(TARGET ReadPositionParser POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:ReadPositionParser> ${PATH_TO_BINARY})

################# Prepare BclExport Plugin ################

add_library(BclExport SHARED plugins/BclExport/BclExport.cpp)
target_compile_definitions(BclExport PUBLIC -DTEST_VIRTUAL=)
target_link_libraries(BclExport PluginLibrary ${Boost_LIBRARIES})
set_target_properties(BclExport PROPERTIES SUFFIX .so)

add_custom_command(TARGET BclExport POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:BclExport> ${PATH_TO_BINARY})

################# Prepare main Executable #################

add_executable(main ./framework/main.cpp)
set_target_properties(main PROPERTIES POSITION_INDEPENDENT_CODE ON)
target_link_libraries(main Build_FrameworkLibs dl)

###########################################################
################### Setup Code Coverage ###################
###########################################################

set(COVERAGE_LCOV_EXCLUDES '/usr/*' '*/tests/*' '*/v1/*')
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake-modules")

if (NOT APPLE) # TODO: Long term fix for profilier errors on MacOS
    if (CMAKE_BUILD_TYPE MATCHES Debug)
        set(COVERAGE_LCOV_EXCLUDES '/usr/*' '*/tests/*' '*/v1/*' '*/plugins/*')
        include(CodeCoverage)
        APPEND_COVERAGE_COMPILER_FLAGS()

        if (GCOV_PATH AND LCOV_PATH AND GENHTML_PATH)
            SETUP_TARGET_FOR_COVERAGE_LCOV(
                    NAME testrunner_coverage # New target name
                    EXECUTABLE UNIT_TESTS    #-j ${PROCESSOR_COUNT} # Executable in PROJECT_BINARY_DIR
                    DEPENDENCIES UNIT_TESTS  # Dependencies to build first
            )
        endif ()
    endif ()
endif ()

###########################################################
###################### Setup Tests ########################
###########################################################

################# Prepare Test Libraries ##################

# Prepare "FakeIt" library target
# FakeIt is a mocking library used for unit-testing in this project
set(FAKEIT_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/tests/fakeit)
add_library(FakeIt INTERFACE) # Defines the target
target_include_directories(FakeIt INTERFACE ${FAKEIT_INCLUDE_DIR})

# Prepare "Catch" library target
# Catch is the unit-testing library used in this project
set(CATCH_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/tests/catch2)
add_library(Catch INTERFACE) # Defines the target
target_include_directories(Catch INTERFACE ${CATCH_INCLUDE_DIR})

######## Build UNIT_TESTS Executable (with Libraries) ######

# Test_FrameworkLibs is the same as Build_FrameworkLibs, but with different preprocessor flags (for testing)
add_library(Test_FrameworkLibs ${FRAMEWORK_SOURCES})
target_compile_definitions(Test_FrameworkLibs PUBLIC protected=public TEST_DEFINITIONS -DTEST_VIRTUAL=virtual)
target_link_libraries(Test_FrameworkLibs
        ${CompressionLibs}
        ${SEQAN_LIBRARIES}
        ${Boost_LIBRARIES}
        ${PYTHON_LIBRARIES}
        dl)

# Collect the source file paths from the test target recursively
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/tests") # Sets `TESTS_SOURCES`

add_library(Test_SerializableIndex serializables/SerializableIndex.cpp)
target_link_libraries(Test_SerializableIndex Test_FrameworkLibs)

add_library(Test_SerializableSequenceElements serializables/SerializableSequenceElements.cpp)
target_link_libraries(Test_SerializableSequenceElements Test_FrameworkLibs)

# Make test executable
add_executable(UNIT_TESTS ${CMAKE_CURRENT_SOURCE_DIR}/tests/main.cpp ${TESTS_SOURCES})
target_link_libraries(UNIT_TESTS
        Catch Test_FrameworkLibs
        Test_SerializableIndex
        Test_SerializableSequenceElements
        SerializableFactory)

# Make Bcl2FastQ plugin Tests executable
add_executable(TEST_Bcl2FastQPlugin
        ${CMAKE_CURRENT_SOURCE_DIR}/tests/Bcl2FastQTests/bcl2FastQPluginTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/tests/main.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/Bcl2FastQ/bcl2FastQ.cpp)
target_link_libraries(TEST_Bcl2FastQPlugin Test_FrameworkLibs)

# Make ReadPositionParser plugin Tests executable
add_executable(TEST_ReadPositionParserPlugin
        ${CMAKE_CURRENT_SOURCE_DIR}/tests/ReadPositionParserTests/ReadPositionParserPluginTests.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/tests/main.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/plugins/ReadPositionParser/ReadPositionParser.cpp)
target_link_libraries(TEST_ReadPositionParserPlugin Test_FrameworkLibs)
